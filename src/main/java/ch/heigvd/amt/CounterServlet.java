package ch.heigvd.amt;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CounterServlet extends HttpServlet {

    private int counter = 0;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String command = request.getParameter("command");
        if (command != null && command.equals("reset")) {
            counter = 0;
        } else {
            synchronized (this) {
                counter = counter + 1;
            }
        }

        response.getWriter().println("This page has been accessed " + counter + " times");
    }

}
